// radix_notation.cc

#include <string>
#include <iostream>

using namespace std;

// Implement this function.
string RadixNotation(unsigned int number, unsigned int radix);

int main() {
  while (true) {
    unsigned int number, radix;
    cin >> number >> radix;
    if (radix < 2 || radix > 36) break;
    cout << RadixNotation(number, radix) << endl;
  }
  return 0;
}
